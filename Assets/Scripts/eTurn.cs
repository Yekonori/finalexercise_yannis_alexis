﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eTurn
{
    PLAYER_ONE,
    PLAYER_TWO,
    ENEMY,
}
